CC  = gcc
MPS = myps
HTP = htop
MSH = sh
FLG = -Wall

all : $(MSH) $(MPS) $(HTP)

$(MSH) : $(MSH).c
	$(CC) $(MSH).c -o $(MSH) $(FLG)

$(MPS) : $(MPS).c
	$(CC) $(MPS).c -o $(MPS) $(FLG)

$(HTP) : $(HTP).c
	$(CC) $(HTP).c -o $(HTP) $(FLG)

clear :
	rm *.o sh myps htop
