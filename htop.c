#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>

#define MAXBUFF 999

#define MAXTAM 70000

// Ponteiro para a estrutura proc
typedef struct processo *pproc;

// Estrutura que guarda cada processo
typedef struct processo
{
        int id;
        int pai;
        int tabs;
        char name[20];
        char state[5];
        char user[20];
        pproc prox;
        pproc filho;
} proc;

// Lista de ponteiros para facil a arvore de processos
pproc procs[MAXTAM];

// Cria e inicializa um novo processo
pproc novo_proc (int id, int pai, char* name, char* state, char* user)
{
        pproc novo = (pproc)malloc(sizeof(proc));
        novo->id = id;
        strcpy(novo->name, name);
        strcpy(novo->state, state);
        strcpy(novo->user, user);
        novo->pai =  procs[pai]->id;
        novo->tabs = procs[pai]->tabs + 1;
        novo->prox = NULL;
        novo->filho = NULL;

        return novo;
}

// Cria e insere um novo processo na arvore de processos
void insere (int id, int pai, char* name, char* state, char* user)
{
        // Se o pai não tem filhos, insere o primeiro
        if (procs[pai]->filho == NULL || procs[pai]->filho == (void*)0x8000)
        {
                procs[id] = procs[pai]->filho = novo_proc (id, pai, name, state, user);
        }
        // Caso tenha filhos insere como o ultimo da fila de irmãos
        else
        {
                pproc aux = procs[pai]->filho;
                while (aux->prox != NULL) aux = aux->prox;

                procs[id] = aux->prox = novo_proc (id, pai, name, state, user);
        }
}

// Imprime todos os processos na ordem: processo, filhos, irmãos
void imprime_proc (pproc processo)
{
        if (processo != NULL)
        {
                // Imprime uma tabela de processos
                printf("%5d | %16s | %20s | %3s    |\n",
                        processo->id, processo->user, processo->name, processo->state);


                if (processo->filho != NULL) {
                        imprime_proc (processo->filho);
                }

                if (processo->prox != NULL) {
                        imprime_proc (processo->prox);
                }

                free(processo);
        }

}

void imprime (pproc processo)
{
        printf("PID   | User             | PROCNAME             | Estado |\n");
        printf("----------------------------------------------------------\n");
        if (processo != NULL)
        {
                imprime_proc (processo);
        }
}

// Programa principal
int htop (void)
{
        // Variaveis utilizadas
        FILE *fp, *userfp;
        char buffer[MAXBUFF];
        char buffer2[MAXBUFF];
        int proc = 0;
        char process[30];
        char *prefix = "/proc/";
        char *sufix_p = "/stat";
        char user_path[50];
        char *sufix_u = "/status";
        DIR* diretory;
        struct dirent* diretory_item;

        int pid;
        char name[20];
        char state[5];
        char user[20];
        int ppid;
        int user_id;

        int i;

        // Inicializa o processo root (init)
        procs[0] = (pproc)malloc(sizeof(proc));
        procs[0]->id = 0;
        procs[0]->pai = 0;
        procs[0]->tabs = -1;

        while (1)
        {
                procs[0]->prox = NULL;
                procs[0]->filho = NULL;

                // Abre o diretorio "/proc/"
                if((diretory = opendir( "/proc/" )) != NULL)
                {
                        // Entra em cada pasta do diretorio
                        diretory_item = readdir( diretory );
                        while( diretory_item )
                        {
                                // Ignora todas as pastas que não sao de processo
                                if (*(diretory_item->d_name) == '1')
                                {
                                        proc = 1;
                                }
                                if (proc) {
                                        process[0] = '\0';
                                        strcat(process, prefix);
                                        strcat(process, diretory_item->d_name);
                                        strcat(process, sufix_p);
                                        user_path[0] = '\0';
                                        strcat(user_path, prefix);
                                        strcat(user_path, diretory_item->d_name);
                                        strcat(user_path, sufix_u);

                                        // Para cada processo le o arquivo "stat" e
                                        // armazema os dados do processo na arvore
                                        if((fp = fopen(process,"r")) != NULL)
                                        {
                                                if(fgets(buffer,MAXBUFF,fp) != NULL)
                                                {
                                                        pid = atoi(strtok (buffer," "));
                                                        strcpy(name, strtok (NULL," ()"));
                                                        strcpy(state, strtok (NULL," ()"));
                                                        ppid  = atoi(strtok (NULL," "));

                                                }

                                                // Le e armazena o codigo do usuario de cada processo
                                                if((userfp = fopen(user_path,"r")) != NULL)
                                                {
                                                        for(i=0; i<8; i++)
                                                        fgets(buffer2,MAXBUFF,userfp);

                                                        strtok (buffer2,":");
                                                        user_id  = atoi(strtok (NULL,""));
                                                }
                                                if(userfp) fclose(userfp);

                                                // Procura o nome do usuario no arquivo "/etc/passwd"
                                                if((userfp = fopen("/etc/passwd","r")) != NULL)
                                                {
                                                        while(fgets(buffer2,MAXBUFF,userfp) != NULL)
                                                        {
                                                                strcpy(user, strtok (buffer2,":"));
                                                                strtok (NULL,":");
                                                                i = atoi(strtok (NULL,":"));
                                                                if (i == user_id) break;
                                                        }
                                                }
                                                if(userfp) fclose(userfp);

                                                insere (pid, ppid, name, state, user);

                                                if(fp) fclose(fp);
                                        }
                                }
                                diretory_item = readdir( diretory );
                        }
                        closedir(diretory);
                        proc = 0;

                        // imprime a arvore de processos
                        imprime (procs[1]);
                }
                // Atualiza Tabela de processos a cada 2s
                sleep(2);
        }

        return 0;
}

int main (void)
{
        htop();

        return 0;
}
