





<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">



  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-49f1e970452082ece91a8cb77754f31a769167f4f9cd527a501b1cafa52bb1b6.css" media="all" rel="stylesheet" />
  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-ce3ebe5d1dd355f0392e6e28517d527ed818deb403150d4e1d697f4642222988.css" media="all" rel="stylesheet" />
  
  
  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/site-a59bd8fb44017386caff9fbca36994bddc4e2da35d6b61b41f39734d64cf6bb6.css" media="all" rel="stylesheet" />
  

  <meta name="viewport" content="width=device-width">
  
  <title>SO-2017-1/README.md at master · flaviovdf/SO-2017-1 · GitHub</title>
  <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
  <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
  <meta property="fb:app_id" content="1401488693436528">

    
    <meta content="https://avatars2.githubusercontent.com/u/521456?v=3&amp;s=400" property="og:image" /><meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="flaviovdf/SO-2017-1" property="og:title" /><meta content="https://github.com/flaviovdf/SO-2017-1" property="og:url" /><meta content="SO-2017-1 - Disciplina de Sistemas Operacionais 2017-1" property="og:description" />

  <link rel="assets" href="https://assets-cdn.github.com/">
  
  <meta name="pjax-timeout" content="1000">
  
  <meta name="request-id" content="BD6C:2F62:FF5697:15D617B:59412920" data-pjax-transient>
  

  <meta name="selected-link" value="repo_source" data-pjax-transient>

  <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
<meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
    <meta name="google-analytics" content="UA-3769691-2">

<meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" /><meta content="https://collector.githubapp.com/github-external/browser_event" name="octolytics-event-url" /><meta content="BD6C:2F62:FF5697:15D617B:59412920" name="octolytics-dimension-request_id" /><meta content="iad" name="octolytics-dimension-region_edge" /><meta content="iad" name="octolytics-dimension-region_render" />
<meta content="/&lt;user-name&gt;/&lt;repo-name&gt;/blob/show" data-pjax-transient="true" name="analytics-location" />




  <meta class="js-ga-set" name="dimension1" content="Logged Out">


  

      <meta name="hostname" content="github.com">
  <meta name="user-login" content="">

      <meta name="expected-hostname" content="github.com">
    <meta name="js-proxy-site-detection-payload" content="M2Q4MDQxY2Q2MzI1YTI0N2RkYmVjZjE1MDE4N2E2MGRjNzA3NjIxMjNjYTY0MjY2YTNjZDc0NGRkZWRjNzY5YXx7InJlbW90ZV9hZGRyZXNzIjoiMTg5LjQwLjg2LjIxIiwicmVxdWVzdF9pZCI6IkJENkM6MkY2MjpGRjU2OTc6MTVENjE3Qjo1OTQxMjkyMCIsInRpbWVzdGFtcCI6MTQ5NzQ0MjU5MiwiaG9zdCI6ImdpdGh1Yi5jb20ifQ==">


  <meta name="html-safe-nonce" content="41fad528257ba6f6651cb467b749b3cb4095dead">

  <meta http-equiv="x-pjax-version" content="8bdcce760611da3548495ddf59e23177">
  

      <link href="https://github.com/flaviovdf/SO-2017-1/commits/master.atom" rel="alternate" title="Recent Commits to SO-2017-1:master" type="application/atom+xml">

  <meta name="description" content="SO-2017-1 - Disciplina de Sistemas Operacionais 2017-1">
  <meta name="go-import" content="github.com/flaviovdf/SO-2017-1 git https://github.com/flaviovdf/SO-2017-1.git">

  <meta content="521456" name="octolytics-dimension-user_id" /><meta content="flaviovdf" name="octolytics-dimension-user_login" /><meta content="84364733" name="octolytics-dimension-repository_id" /><meta content="flaviovdf/SO-2017-1" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="84364733" name="octolytics-dimension-repository_network_root_id" /><meta content="flaviovdf/SO-2017-1" name="octolytics-dimension-repository_network_root_nwo" /><meta content="false" name="octolytics-dimension-repository_explore_github_marketplace_ci_cta_shown" />


    <link rel="canonical" href="https://github.com/flaviovdf/SO-2017-1/blob/master/tp1/README.md" data-pjax-transient>


  <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">

  <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">

  <link rel="mask-icon" href="https://assets-cdn.github.com/pinned-octocat.svg" color="#000000">
  <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

<meta name="theme-color" content="#1e2327">



  </head>

  <body class="logged-out env-production page-blob">
    



  <div class="position-relative js-header-wrapper ">
    <a href="#start-of-content" tabindex="1" class="px-2 py-4 show-on-focus js-skip-to-content">Skip to content</a>
    <div id="js-pjax-loader-bar" class="pjax-loader-bar"><div class="progress"></div></div>

    
    
    



          <header class="site-header js-details-container Details" role="banner">
  <div class="site-nav-container">
    <a class="header-logo-invertocat" href="https://github.com/" aria-label="Homepage" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="32" version="1.1" viewBox="0 0 16 16" width="32"><path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"/></svg>
    </a>

    <button class="btn-link float-right site-header-toggle js-details-target" type="button" aria-label="Toggle navigation">
      <svg aria-hidden="true" class="octicon octicon-three-bars" height="24" version="1.1" viewBox="0 0 12 16" width="18"><path fill-rule="evenodd" d="M11.41 9H.59C0 9 0 8.59 0 8c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zm0-4H.59C0 5 0 4.59 0 4c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM.59 11H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1H.59C0 13 0 12.59 0 12c0-.59 0-1 .59-1z"/></svg>
    </button>

    <div class="site-header-menu">
      <nav class="site-header-nav">
        <a href="/features" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:features" data-selected-links="/features /features">
          Features
</a>        <a href="/business" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:business" data-selected-links="/business /business/security /business/customers /business">
          Business
</a>        <a href="/explore" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:explore" data-selected-links="/explore /trending /trending/developers /integrations /integrations/feature/code /integrations/feature/collaborate /integrations/feature/ship /showcases /explore">
          Explore
</a>            <a href="/marketplace" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:marketplace" data-selected-links=" /marketplace">
              Marketplace
</a>        <a href="/pricing" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:pricing" data-selected-links="/pricing /pricing/developer /pricing/team /pricing/business-hosted /pricing/business-enterprise /pricing">
          Pricing
</a>      </nav>

      <div class="site-header-actions">
          <div class="header-search scoped-search site-scoped-search js-site-search" role="search">
  <!-- '"` --><!-- </textarea></xmp> --></option></form><form accept-charset="UTF-8" action="/flaviovdf/SO-2017-1/search" class="js-site-search-form" data-scoped-search-url="/flaviovdf/SO-2017-1/search" data-unscoped-search-url="/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <label class="form-control header-search-wrapper js-chromeless-input-container">
        <a href="/flaviovdf/SO-2017-1/blob/master/tp1/README.md" class="header-search-scope no-underline">This repository</a>
      <input type="text"
        class="form-control header-search-input js-site-search-focus js-site-search-field is-clearable"
        data-hotkey="s"
        name="q"
        value=""
        placeholder="Search"
        aria-label="Search this repository"
        data-unscoped-placeholder="Search GitHub"
        data-scoped-placeholder="Search"
        autocapitalize="off">
        <input type="hidden" class="js-site-search-type-field" name="type" >
    </label>
</form></div>


          <a class="text-bold site-header-link" href="/login?return_to=%2Fflaviovdf%2FSO-2017-1%2Fblob%2Fmaster%2Ftp1%2FREADME.md" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
            <span class="text-gray">or</span>
            <a class="text-bold site-header-link" href="/join?source=header-repo" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
      </div>
    </div>
  </div>
</header>


  </div>

  <div id="start-of-content" class="show-on-focus"></div>

    <div id="js-flash-container">
</div>



  <div role="main">
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode">
    <div id="js-repo-pjax-container" data-pjax-container>
      


  


    <div class="pagehead repohead instapaper_ignore readability-menu experiment-repo-nav">
      <div class="container repohead-details-container">

        <ul class="pagehead-actions">
  <li>
      <a href="/login?return_to=%2Fflaviovdf%2FSO-2017-1"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6C13 14 16 8 16 8s-3-6-7.94-6zM8 12c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm2-4c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z"/></svg>
    Watch
  </a>
  <a class="social-count" href="/flaviovdf/SO-2017-1/watchers"
     aria-label="4 users are watching this repository">
    4
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fflaviovdf%2FSO-2017-1"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z"/></svg>
    Star
  </a>

    <a class="social-count js-social-count" href="/flaviovdf/SO-2017-1/stargazers"
      aria-label="2 users starred this repository">
      2
    </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fflaviovdf%2FSO-2017-1"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <svg aria-hidden="true" class="octicon octicon-repo-forked" height="16" version="1.1" viewBox="0 0 10 16" width="10"><path fill-rule="evenodd" d="M8 1a1.993 1.993 0 0 0-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V6.5l3 3v1.78A1.993 1.993 0 0 0 5 15a1.993 1.993 0 0 0 1-3.72V9.5l3-3V4.72A1.993 1.993 0 0 0 8 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"/></svg>
        Fork
      </a>

    <a href="/flaviovdf/SO-2017-1/network" class="social-count"
       aria-label="1 user forked this repository">
      1
    </a>
  </li>
</ul>

        <h1 class="public ">
  <svg aria-hidden="true" class="octicon octicon-repo" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M4 9H3V8h1v1zm0-3H3v1h1V6zm0-2H3v1h1V4zm0-2H3v1h1V2zm8-1v12c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1zm-1 10H1v2h2v-1h3v1h5v-2zm0-10H2v9h9V1z"/></svg>
  <span class="author" itemprop="author"><a href="/flaviovdf" class="url fn" rel="author">flaviovdf</a></span><!--
--><span class="path-divider">/</span><!--
--><strong itemprop="name"><a href="/flaviovdf/SO-2017-1" data-pjax="#js-repo-pjax-container">SO-2017-1</a></strong>

</h1>

      </div>
      <div class="container">
        
<nav class="reponav js-repo-nav js-sidenav-container-pjax"
     itemscope
     itemtype="http://schema.org/BreadcrumbList"
     role="navigation"
     data-pjax="#js-repo-pjax-container">

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/flaviovdf/SO-2017-1" class="js-selected-navigation-item selected reponav-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /flaviovdf/SO-2017-1" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-code" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"/></svg>
      <span itemprop="name">Code</span>
      <meta itemprop="position" content="1">
</a>  </span>

    <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
      <a href="/flaviovdf/SO-2017-1/issues" class="js-selected-navigation-item reponav-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /flaviovdf/SO-2017-1/issues" itemprop="url">
        <svg aria-hidden="true" class="octicon octicon-issue-opened" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z"/></svg>
        <span itemprop="name">Issues</span>
        <span class="Counter">0</span>
        <meta itemprop="position" content="2">
</a>    </span>

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/flaviovdf/SO-2017-1/pulls" class="js-selected-navigation-item reponav-item" data-hotkey="g p" data-selected-links="repo_pulls /flaviovdf/SO-2017-1/pulls" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-git-pull-request" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M11 11.28V5c-.03-.78-.34-1.47-.94-2.06C9.46 2.35 8.78 2.03 8 2H7V0L4 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 10 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zM4 3c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v6.56A1.993 1.993 0 0 0 2 15a1.993 1.993 0 0 0 1-3.72V4.72c.59-.34 1-.98 1-1.72zm-.8 10c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"/></svg>
      <span itemprop="name">Pull requests</span>
      <span class="Counter">0</span>
      <meta itemprop="position" content="3">
</a>  </span>

    <a href="/flaviovdf/SO-2017-1/projects" class="js-selected-navigation-item reponav-item" data-selected-links="repo_projects new_repo_project repo_project /flaviovdf/SO-2017-1/projects">
      <svg aria-hidden="true" class="octicon octicon-project" height="16" version="1.1" viewBox="0 0 15 16" width="15"><path fill-rule="evenodd" d="M10 12h3V2h-3v10zm-4-2h3V2H6v8zm-4 4h3V2H2v12zm-1 1h13V1H1v14zM14 0H1a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h13a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1z"/></svg>
      Projects
      <span class="Counter" >0</span>
</a>


    <div class="reponav-dropdown js-menu-container">
      <button type="button" class="btn-link reponav-item reponav-dropdown js-menu-target " data-no-toggle aria-expanded="false" aria-haspopup="true">
        Insights
        <svg aria-hidden="true" class="octicon octicon-triangle-down v-align-middle text-gray" height="11" version="1.1" viewBox="0 0 12 16" width="8"><path fill-rule="evenodd" d="M0 5l6 6 6-6z"/></svg>
      </button>
      <div class="dropdown-menu-content js-menu-content">
        <div class="dropdown-menu dropdown-menu-sw">
          <a class="dropdown-item" href="/flaviovdf/SO-2017-1/pulse" data-skip-pjax>
            <svg aria-hidden="true" class="octicon octicon-pulse" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0v2h3.6l.9-1.8.9 5.4L9 8.5l1.6 1.5H14V8z"/></svg>
            Pulse
          </a>
          <a class="dropdown-item" href="/flaviovdf/SO-2017-1/graphs" data-skip-pjax>
            <svg aria-hidden="true" class="octicon octicon-graph" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z"/></svg>
            Graphs
          </a>
        </div>
      </div>
    </div>
</nav>

      </div>
    </div>

<div class="container new-discussion-timeline experiment-repo-nav">
  <div class="repository-content">

    
    
  <a href="/flaviovdf/SO-2017-1/blob/c0a25bc582517da06d7165218564eb0781c5d14b/tp1/README.md" class="d-none js-permalink-shortcut" data-hotkey="y">Permalink</a>

  <!-- blob contrib key: blob_contributors:v21:b887ef28e6b6e4879f4c0708d624ba0e -->

  <div class="file-navigation js-zeroclipboard-container">
    
<div class="select-menu branch-select-menu js-menu-container js-select-menu float-left">
  <button class=" btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    
    type="button" aria-label="Switch branches or tags" aria-expanded="false" aria-haspopup="true">
      <i>Branch:</i>
      <span class="js-select-button css-truncate-target">master</span>
  </button>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax>

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
        <span class="select-menu-title">Switch branches/tags</span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="form-control js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/flaviovdf/SO-2017-1/blob/master/tp1/README.md"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"/></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text">
                master
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

    <div class="BtnGroup float-right">
      <a href="/flaviovdf/SO-2017-1/find/master"
            class="js-pjax-capture-input btn btn-sm BtnGroup-item"
            data-pjax
            data-hotkey="t">
        Find file
      </a>
      <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm BtnGroup-item tooltipped tooltipped-s" data-copied-hint="Copied!" type="button">Copy path</button>
    </div>
    <div class="breadcrumb js-zeroclipboard-target">
      <span class="repo-root js-repo-root"><span class="js-path-segment"><a href="/flaviovdf/SO-2017-1"><span>SO-2017-1</span></a></span></span><span class="separator">/</span><span class="js-path-segment"><a href="/flaviovdf/SO-2017-1/tree/master/tp1"><span>tp1</span></a></span><span class="separator">/</span><strong class="final-path">README.md</strong>
    </div>
  </div>


  
  <div class="commit-tease">
      <span class="float-right">
        <a class="commit-tease-sha" href="/flaviovdf/SO-2017-1/commit/8fdf6ffd4ab32d5e022ec8073559d289077f1014" data-pjax>
          8fdf6ff
        </a>
        <relative-time datetime="2017-03-19T18:22:20Z">Mar 19, 2017</relative-time>
      </span>
      <div>
        <img alt="@flaviovdf" class="avatar" height="20" src="https://avatars1.githubusercontent.com/u/521456?v=3&amp;s=40" width="20" />
        <a href="/flaviovdf" class="user-mention" rel="author">flaviovdf</a>
          <a href="/flaviovdf/SO-2017-1/commit/8fdf6ffd4ab32d5e022ec8073559d289077f1014" class="message" data-pjax="true" title="Update README.md">Update README.md</a>
      </div>

    <div class="commit-tease-contributors">
      <button type="button" class="btn-link muted-link contributors-toggle" data-facebox="#blob_contributors_box">
        <strong>1</strong>
         contributor
      </button>
      
    </div>

    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header" data-facebox-id="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list" data-facebox-id="facebox-description">
          <li class="facebox-user-list-item">
            <img alt="@flaviovdf" height="24" src="https://avatars3.githubusercontent.com/u/521456?v=3&amp;s=48" width="24" />
            <a href="/flaviovdf">flaviovdf</a>
          </li>
      </ul>
    </div>
  </div>

  <div class="file">
    <div class="file-header">
  <div class="file-actions">

    <div class="BtnGroup">
      <a href="/flaviovdf/SO-2017-1/raw/master/tp1/README.md" class="btn btn-sm BtnGroup-item" id="raw-url">Raw</a>
        <a href="/flaviovdf/SO-2017-1/blame/master/tp1/README.md" class="btn btn-sm js-update-url-with-hash BtnGroup-item" data-hotkey="b">Blame</a>
      <a href="/flaviovdf/SO-2017-1/commits/master/tp1/README.md" class="btn btn-sm BtnGroup-item" rel="nofollow">History</a>
    </div>


        <button type="button" class="btn-octicon disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-pencil" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 0 1 1.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z"/></svg>
        </button>
        <button type="button" class="btn-octicon btn-octicon-danger disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-trashcan" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"/></svg>
        </button>
  </div>

  <div class="file-info">
      357 lines (250 sloc)
      <span class="file-info-divider"></span>
    11.7 KB
  </div>
</div>

    
  <div id="readme" class="readme blob instapaper_body">
    <article class="markdown-body entry-content" itemprop="text"><h1><a id="user-content-tp1-shell--ps-top--sinais--módulos" class="anchor" href="#tp1-shell--ps-top--sinais--módulos" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>TP1: Shell + PS (TOP) + Sinais + Módulos</h1>
<ol>
<li>Entrega no mesmo dia da Primeira Prova</li>
<li>Pode ser feito em dupla</li>
</ol>
<p>Parte deste material foi adaptado do material do <a href="http://dcc.ufmg.br/%7Ecunha">Prof. Italo Cunha</a></p>
<p>Neste TP vamos explorar alguns conceitos da primeira parte da disciplina. Em particular, vamos rever os conceitos de Pipes, Estruturas de Processos do Kernel e Sinais.</p>
<h2><a id="user-content-parte-1-desenvolvendo-um-shell-básico" class="anchor" href="#parte-1-desenvolvendo-um-shell-básico" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 1: Desenvolvendo um Shell Básico</h2>
<p>Neste trabalho você se familiarizará com a interface de chamadas de sistema do Linux implementando algumas funcionalidades num shell simples. Para que você foque apenas na parte de chamadas de sistema, baixe o <a href="https://gitlab.dcc.ufmg.br/cunha-dcc605/shell-assignment">esqueleto</a> do shell e o estude. O esqueleto do shell contém duas partes: um processador de linhas de comando e código para execução dos comandos. Você não precisa modificar o processador de linhas de comando (a não ser que queira implementar algumas das atividades extra abaixo), mas deve completar o código para execução dos comandos. O processador de linhas só reconhece comandos simples como:</p>
<pre><code>$ ls &gt; y
$ cat &lt; y | sort | uniq | wc &gt; y1
$ cat y1
$ rm y1
$ ls | sort | uniq | wc
$ rm y
</code></pre>
<p>Se você não entende o que esses comandos fazem, estude o manual de um shell do Linux (por exemplo, do bash) bem como o manual de cada um dos comandos acima (ls, cat, rm, sort, uniq, wc) para se familiarizar. Copie e cole esses comandos num arquivo, por exemplo, teste.sh.</p>
<p>Você pode compilar o esqueleto do shell rodando:</p>
<pre><code>$ gcc sh.c -o myshell
</code></pre>
<p>Nota: Nesta especificação colocamos um sinal de dólar antes das linhas que devem ser executadas no shell do sistema (por exemplo, o bash). As linhas de comando sem dólar devem ser executadas no shell simplificado que você está implementando.</p>
<p>Esse comando irá produzir um arquivo <code>myshell</code> que você pode rodar:</p>
<pre><code>$ ./myshell
</code></pre>
<p>Para sair do shell simplificado aperte ctrl+d (fim de arquivo). Teste o shell executando os comandos no arquivo teste.sh:</p>
<pre><code>$ ./myshell &lt; teste.sh
</code></pre>
<p>Essa execução irá falhar pois você ainda não implementou várias funcionalidades do shell. É isso que você fará nesse trabalho.
Executando comandos simples
Implemente comandos simples, como:</p>
<pre><code>$ ls
</code></pre>
<p>O processador de linhas já constrói uma estrutura execcmd para você, a única coisa que você precisa fazer é escrever o código do case ' ' (espaço) na função runcmd. Depois de escrever o código, teste execução de programas simples como:</p>
<pre><code>$ ls
$ cat sh.c
</code></pre>
<p>Nota: Você não precisa implementar o código do programa ls; você deve simplesmente implementar as funções no esqueleto do shell simplificado para permitir que ele execute comandos simples como acima.</p>
<p>Se ainda não conhecê-la, dê uma olhada no manual da função exec ($ man 3 exec). Importante: não use a função system para implementar as funções do seu shell.</p>
<p><strong>Redirecionamento de entrada/saída</strong></p>
<p>Implemente comandos com redirecionamento de entrada e saída para que você possa rodar:</p>
<pre><code>$ echo "DCC605 is cool" &gt; x.txt
$ cat &lt; x.txt
</code></pre>
<p>O processador de linhas já reconhece "&gt;" e "&lt;" e constrói uma estrutura redircmd para você. Seu trabalho é apenas preencher o código na função runcmd para esses casos. Teste sua implementação com os comandos acima e outros comandos similares.</p>
<p>Dica: Dê uma olhada no manual das funções open e close (man 2 open). Se você não conhece o esquema de entrada e saída padrão de programas, dê uma olhada no artigo da Wikipedia <a href="https://gitlab.dcc.ufmg.br/cunha-dcc605/shell-assignment">aqui</a>.</p>
<p><strong>Sequenciamento de comandos</strong></p>
<p>Implemente pipes para que você consiga rodar comandos tipo</p>
<pre><code>$ ls | sort | uniq | wc
</code></pre>
<p>O processador de linhas já reconhece '|' e constrói uma estrutura pipecmd pra você. A única coisa que você precisa fazer é completar o código para o case '|' na função runcmd. Teste sua implementação para o comando acima. Se precisar, leia a documentação das funções pipe, fork e close.</p>
<ol>
<li><a href="https://linux.die.net/man/2/fork">https://linux.die.net/man/2/fork</a></li>
<li><a href="https://linux.die.net/man/3/exec">https://linux.die.net/man/3/exec</a></li>
<li><a href="https://linux.die.net/man/2/pipe">https://linux.die.net/man/2/pipe</a></li>
<li><a href="https://linux.die.net/man/2/dup">https://linux.die.net/man/2/dup</a></li>
</ol>
<p><strong>Esclarecimentos</strong></p>
<ol>
<li>Não use a função system na sua implemencação. Use fork e exec.</li>
</ol>
<h2><a id="user-content-parte-2-lendo-o-proc-para-fazer-um-ps-tree" class="anchor" href="#parte-2-lendo-o-proc-para-fazer-um-ps-tree" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 2: Lendo o /proc/ para fazer um PS-Tree</h2>
<p>Agora que você tem o <code>myshell</code> implementado, sua tarefa é desenvolver um comando chamado <code>myps</code>. Neste, você deve ler os arquivos do <code>/proc/*/stat</code> para criar uma ps-tree. O <code>/proc</code> é um diretório de arquivos especiais do linux que lista os processos em execuções. Este arquivo embora possa ser lido como um arquivo normal, na verdade não é um arquivo em disco e sim um file handle para um arquivo especial que lista informações do kernel. Vamos ver mais detalhes sobre tal assunto na segunda parte da disciplina.</p>
<p>Dê uma olhada no manpage do /proc: <a href="http://man7.org/linux/man-pages/man5/proc.5.html">Manpage</a></p>
<p>Outra fonte de dados é o código do ps: <a href="https://github.com/thlorenz/procps/blob/master/deps/procps/proc/readproc.c">PS</a></p>
<p>Imprima a árvore de processos em usando tab como um separados. Novamente, lembre-se que o foco no TP não é gastar tempo em embelezamento de entrada e saída, e sim em testar e aprender sistemas operacionais.</p>
<pre><code>$ ./myps
init
    child1
        child11
            child111
        child12
        child13
    child2
        child21
            child211
    child3
</code></pre>
<p><strong>Dica:</strong> Execute o comando <code>pstree</code> para ter uma noção de como uma árvore é impressa.</p>
<h2><a id="user-content-parte-3-uma-top-simples-topzera" class="anchor" href="#parte-3-uma-top-simples-topzera" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 3: Uma TOP Simples (topzera)</h2>
<p>Vamos agora nos inspirar no comando <code>htop</code> para aprender um pouco mais sobre sinais. O htop é um top avançado no linux que permite o envio de sinais para processos em execução. Você pode usar o seu comado <code>myps</code> como base para o seu comando top.</p>
<p><strong>Código de Teste:</strong> Disponibilizei um código <a href="https://github.com/flaviovdf/SO-2017-1/blob/master/tp1/signaltester/tester.c">signaltester</a> para você testar o seu trabalho. O mesmo faz um tratamento simples de sinais em C.</p>
<p>Compile o teste com a linha</p>
<pre><code>$ gcc signaltester.c -o signaltester
</code></pre>
<p><strong>Topzera</strong></p>
<p>Modifique seu comando PS para imprimir os processos em sequência (remover os tabs). Além disto, altere o mesmo para identificar o PID do programa, o usuário que está executando o mesmo e o estado do processo. Com isto, imprima os programas em execução em uma tabela como a abaixo. Tal tabela deve ser atualizada a cada 1 segundo.</p>
<p>Seu comando deve se chamar <code>topzera</code>.</p>
<pre><code>PID    | User    | PROCNAME | Estado |
-------|---------|----------|--------|
1272   | flavio  | yes      | S      |
1272   | root    | init     | S      |
</code></pre>
<p><strong>Dica</strong> Você pode limitar seu topzera para imprimir apenas os 20 primeiros processos que encontrar.</p>
<h2><a id="user-content-parte-4-sinais" class="anchor" href="#parte-4-sinais" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 4: Sinais</h2>
<p>Permita que seu comando TOP envie sinais. Isto é, crie uma função no seu TOP que enviar sinais para um PID. Tal função pode ser apenas digitar um "PID SINAL". Por exemplo, se o signaltester tem PID 2131, o código abaixo deve enviar o sinal SIGHUP para o mesmo.</p>
<pre><code>PID    | User    | PROCNAME     | Estado |
-------|---------|--------------|--------|
2131   | flavio  | signaltester | S      |
&gt; 2131 1
</code></pre>
<p>Com este sinal o processo deve morrer e sair da sua lista.</p>
<p><strong>Dica:</strong> Possivelmente o signaltester não vai aparecer entre os 20 primeiros. Use o comando abaixo para descobrir o PID do
mesmo e testar seu topzera.</p>
<pre><code>$ ps | grep signalteste
</code></pre>
<h2><a id="user-content-parte-5-criando-um-módulo-linux-que-funciona-similar-ao-ps-tree" class="anchor" href="#parte-5-criando-um-módulo-linux-que-funciona-similar-ao-ps-tree" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 5: Criando um Módulo Linux que funciona similar ao PS-Tree</h2>
<p>O exercício abaixo é feito com base no Projeto de Programação 2 da 9a edição do livro do Siberschatz. Vamos criar um módulo que implementa o PS-Tree.</p>
<p><strong>Listando módulos do kernel</strong></p>
<p>Inicialmente vamos criar um módulo do kernel linux. No caminho, vamos aprender uma série de comandos úteis para lidar com módulo no mesmo. Alguns dos comandos abaixo só funcionarão no modo <em>root</em>, então usaremos o sudo:</p>
<pre><code>$ sudo echo 'Sudo!'
</code></pre>
<p>Para listar módulos do kernel você pode utilizad o comando <code>lsmod</code>.</p>
<pre><code>$ lsmod
</code></pre>
<p>Exemplo de saída:</p>
<pre><code>$ lsmod
Module                  Size  Used by
bnep                   20480  2
ip6table_filter        16384  0
ip6_tables             28672  1 ip6table_filter
iptable_filter         16384  0
ip_tables              24576  1 iptable_filter
</code></pre>
<p><strong>Criando módulos do kernel</strong></p>
<p>Além disto, você vai precisar dos headers do seu kernel. No ubuntu:</p>
<pre><code>$ sudo apt-get install linux-headers-`uname -r`
</code></pre>
<p>O comando <code>uname -r</code> identifica a versão do ser kernel.</p>
<p>O código abaixo é um módulo simples. Note que o mesmo faz uso de <em>printk</em> e não <em>printf</em>. No kernel, não conseguimos chamar funções da biblioteca padrão C.</p>
<div class="highlight highlight-source-c"><pre><span class="pl-c"><span class="pl-c">/*</span></span>
<span class="pl-c"> * This code is based on the code provided by Operating Systems Concepts, 9th</span>
<span class="pl-c"> * edition under a GPL license.</span>
<span class="pl-c"> <span class="pl-c">*/</span></span>
#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/init.h<span class="pl-pds">&gt;</span></span>
#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/kernel.h<span class="pl-pds">&gt;</span></span>
#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/module.h<span class="pl-pds">&gt;</span></span>

<span class="pl-k">int</span>
<span class="pl-en">simple_init</span>(<span class="pl-k">void</span>)
{
  <span class="pl-c1">printk</span>(<span class="pl-s"><span class="pl-pds">"</span>Loading module<span class="pl-cce">\n</span><span class="pl-pds">"</span></span>);
  <span class="pl-k">return</span> <span class="pl-c1">0</span>;
}

<span class="pl-k">void</span>
<span class="pl-en">simple_exit</span>(<span class="pl-k">void</span>)
{
  <span class="pl-c1">printk</span>(<span class="pl-s"><span class="pl-pds">"</span>Removing module<span class="pl-cce">\n</span><span class="pl-pds">"</span></span>);
}

<span class="pl-en">module_init</span>(simple_init);
<span class="pl-en">module_exit</span>(simple_exit);

<span class="pl-en">MODULE_LICENSE</span>(<span class="pl-s"><span class="pl-pds">"</span>GPL<span class="pl-pds">"</span></span>);
<span class="pl-en">MODULE_DESCRIPTION</span>(<span class="pl-s"><span class="pl-pds">"</span>Simple Module<span class="pl-pds">"</span></span>);</pre></div>
<p>A forma mais simples de compilar o módulo acima é usando um Makefile como o exemplo abaixo:</p>
<div class="highlight highlight-source-makefile"><pre><span class="pl-smi">obj-m</span> := simple.o
<span class="pl-smi">KDIR</span> := /lib/modules/<span class="pl-s">$(<span class="pl-c1">shell</span> uname -r)</span>/build
<span class="pl-smi">PWD</span> := <span class="pl-s">$(<span class="pl-c1">shell</span> pwd)</span>

<span class="pl-en">default</span>:
	<span class="pl-s">$(<span class="pl-c1">MAKE</span>)</span> -C <span class="pl-s">$(<span class="pl-smi">KDIR</span>)</span> SUBDIRS=<span class="pl-s">$(<span class="pl-smi">PWD</span>)</span> modules

<span class="pl-en">clean</span>:
	<span class="pl-s">$(<span class="pl-c1">MAKE</span>)</span> -C <span class="pl-s">$(<span class="pl-smi">KDIR</span>)</span> SUBDIRS=<span class="pl-s">$(<span class="pl-smi">PWD</span>)</span> clean</pre></div>
<p>Coloque o <code>Makefile</code> junto com o seu código e digite:</p>
<pre><code>$ make
</code></pre>
<p>Com isto você compilou o seu módulo. Agora você deve ter os seguintes arquivos</p>
<pre><code>$ ls
ls
Makefile       Module.symvers  simple.ko     simple.mod.o
modules.order  simple.c        simple.mod.c  simple.o
</code></pre>
<p>Seu módulo é o <code>simple.ko</code>. Insira ele no kernel com o comando:</p>
<pre><code>$ sudo insmod simple.ko
</code></pre>
<p>Execute o dmesg e verifique se o módulo foi carregado</p>
<pre><code>$ dmesg
</code></pre>
<p>Sua mensagem de init deve ser a última, ou uma das últimas.</p>
<p>Remova o módulo com</p>
<pre><code>$ sudo rmmod simple
</code></pre>
<p>Verifique que o mesmo foi removido com dmesg novamente.</p>
<p><strong>PS-Tree Módulo</strong></p>
<p>Agora vamos olhar as estrtuturas de processos no kernel do linux. O código abaixo faz uso do <a href="https://github.com/torvalds/linux/blob/master/include/linux/sched.h">sched.h</a>. O mesmo simplesmente imprime todas as tarefas em execução no momento.</p>
<p>Preste atenção:</p>
<ol>
<li>Leia o <a href="https://github.com/torvalds/linux/blob/master/include/linux/sched.h">sched.h</a></li>
<li>Note que a iteração é feita com uma macro <code>for_each_process(task)</code></li>
</ol>
<div class="highlight highlight-source-c"><pre>#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/init.h<span class="pl-pds">&gt;</span></span>
#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/kernel.h<span class="pl-pds">&gt;</span></span>
#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/sched.h<span class="pl-pds">&gt;</span></span>
#<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>linux/module.h<span class="pl-pds">&gt;</span></span>

<span class="pl-k">int</span>
<span class="pl-en">ps_init</span>(<span class="pl-k">void</span>)
{
  <span class="pl-k">struct</span> task_struct *task;
  <span class="pl-c1">printk</span>(KERN_INFO <span class="pl-s"><span class="pl-pds">"</span>Loading module myps<span class="pl-cce">\n</span><span class="pl-pds">"</span></span>);
  <span class="pl-c1">for_each_process</span>(task) {
    <span class="pl-c1">printk</span>(<span class="pl-s"><span class="pl-pds">"</span>Name: <span class="pl-c1">%s</span> PID: [<span class="pl-c1">%d</span>]<span class="pl-cce">\n</span><span class="pl-pds">"</span></span>, task-&gt;comm, task-&gt;pid);
  }
  <span class="pl-k">return</span> <span class="pl-c1">0</span>;
}

<span class="pl-k">void</span>
<span class="pl-en">ps_exit</span>(<span class="pl-k">void</span>)
{
  <span class="pl-c1">printk</span>(KERN_INFO <span class="pl-s"><span class="pl-pds">"</span>Removing module myps<span class="pl-cce">\n</span><span class="pl-pds">"</span></span>);
}

<span class="pl-en">module_init</span>(ps_init);
<span class="pl-en">module_exit</span>(ps_exit);

<span class="pl-en">MODULE_LICENSE</span>(<span class="pl-s"><span class="pl-pds">"</span>GPL<span class="pl-pds">"</span></span>);
<span class="pl-en">MODULE_DESCRIPTION</span>(<span class="pl-s"><span class="pl-pds">"</span>PS Module<span class="pl-pds">"</span></span>);</pre></div>
<p>Usando as macros abaixo tente criar o PSTree:</p>
<div class="highlight highlight-source-c"><pre><span class="pl-k">struct</span> task_struct *task;
<span class="pl-k">struct</span> list_head *list;
<span class="pl-en">list_for_each</span>(list, &amp;init_task-&gt;children) {
  task = <span class="pl-c1">list_entry</span>(list, <span class="pl-k">struct</span> task_struct, sibling);
  <span class="pl-c"><span class="pl-c">/*</span> task points to the next child in the list <span class="pl-c">*/</span></span>
}</pre></div>
<p>A macro <code>list_for_each</code> recebe dois parâmetros, ambos do tipo <code>struct list_head</code>:</p>
<ol>
<li>Um ponteiro para onde vamos armazenar a cabeça da lista</li>
<li>Um ponteiro para os filhos do init_task, vamos percorrer os mesmos</li>
</ol>
<p>A macro <code>list_entry</code> retorna cada elemento da lista.</p>
<ol>
<li><a href="https://github.com/flaviovdf/SO-2017-1/blob/master/tp1/kernel/">Código exemplo</a></li>
<li><a href="https://en.wikipedia.org/wiki/Printk">https://en.wikipedia.org/wiki/Printk</a></li>
</ol>
</article>
  </div>

  </div>

  <button type="button" data-facebox="#jump-to-line" data-facebox-class="linejump" data-hotkey="l" class="d-none">Jump to Line</button>
  <div id="jump-to-line" style="display:none">
    <!-- '"` --><!-- </textarea></xmp> --></option></form><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
      <input class="form-control linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" aria-label="Jump to line" autofocus>
      <button type="submit" class="btn">Go</button>
</form>  </div>


  </div>
  <div class="modal-backdrop js-touch-events"></div>
</div>


    </div>
  </div>

  </div>

      
<div class="container site-footer-container">
  <div class="site-footer " role="contentinfo">
    <ul class="site-footer-links float-right">
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact GitHub</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage" class="site-footer-mark" title="GitHub">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="24" version="1.1" viewBox="0 0 16 16" width="24"><path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"/></svg>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2017 <span title="0.06018s from github-fe-1e914f7.cp1-iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>
    </ul>
  </div>
</div>



  <div id="ajax-error-message" class="ajax-error-message flash flash-error">
    <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"/></svg>
    <button type="button" class="flash-close js-flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
    </button>
    You can't perform that action at this time.
  </div>


    <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/compat-8a4318ffea09a0cdb8214b76cf2926b9f6a0ced318a317bed419db19214c690d.js"></script>
    <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-73720f027bb317fceb118c259275da4be5efa344c246a12341a68c3168ceeaa7.js"></script>
    
    <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github-d379af1e588755eb10c26c968d64e3b72b32c13ff9dbb890e19c2c3b5ba17c60.js"></script>
    
    
    
    
  <div class="js-stale-session-flash stale-session-flash flash flash-warn flash-banner d-none">
    <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"/></svg>
    <span class="signed-in-tab-flash">You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
    <span class="signed-out-tab-flash">You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
  </div>
  <div class="facebox" id="facebox" style="display:none;">
  <div class="facebox-popup">
    <div class="facebox-content" role="dialog" aria-labelledby="facebox-header" aria-describedby="facebox-description">
    </div>
    <button type="button" class="facebox-close js-facebox-close" aria-label="Close modal">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
    </button>
  </div>
</div>


  </body>
</html>

