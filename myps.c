#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <dirent.h>
#include <string.h>

#define MAXBUFF 999

#define MAXTAM 70000

// Ponteiro para a estrutura proc
typedef struct processo *pproc;

// Estrutura que guarda cada processo
typedef struct processo
{
        int id;
        int pai;
        int tabs;
        char name[20];
        char state[5];
        pproc prox;
        pproc filho;
} proc;

// Lista de ponteiros para facil a arvore de processos
pproc procs[MAXTAM];

// Cria e inicializa um novo processo
pproc novo_proc (int id, int pai, char* name, char* state)
{
        pproc novo = (pproc)malloc(sizeof(proc));
        novo->id = id;
        strcpy(novo->name, name);
        strcpy(novo->state, state);
        novo->pai =  procs[pai]->id;
        novo->tabs = procs[pai]->tabs + 1;
        novo->prox = NULL;
        novo->filho = NULL;

        return novo;
}

// Cria e insere um novo processo na arvore de processos
void insere (int id, int pai, char* name, char* state)
{
        // Se o pai não tem filhos, insere o primeiro
        if (procs[pai]->filho == NULL || procs[pai]->filho == (void*)0x8000)
        {
                procs[id] = procs[pai]->filho = novo_proc (id, pai, name, state);
        }
        // Caso tenha filhos insere como o ultimo da fila de irmãos
        else
        {
                pproc aux = procs[pai]->filho;
                while (aux->prox != NULL) aux = aux->prox;

                procs[id] = aux->prox = novo_proc (id, pai, name, state);
        }
}

// Imprime todos os processos na ordem: processo, filhos, irmãos
void imprime (pproc processo)
{
        if (processo != NULL)
        {
                if (processo->id != 0)
                {
                        // Imprime o filho deslocado a direita do pai
                        for (int i = 0; i < processo->tabs-1; i++)
                        {
                                printf("\t");
                        }
                        if ( processo->tabs > 0)
                        {
                                printf("   L--> ");
                        }
                        printf("%s\n", processo->name);
                }

                if (processo->filho != NULL) {
                        imprime (processo->filho);
                }

                if (processo->prox != NULL) {
                        imprime (processo->prox);
                }

                free(processo);
        }

}

// Programa principal
int myps (void)
{
        // Variaveis utilizadas
        FILE *fp;
        char buffer[MAXBUFF];
        int proc = 0;
        char *prefix = "/proc/";
        char *sufix = "/stat";
        char process[30];
        DIR* diretory;
        struct dirent* diretory_item;

        int pid;
        char name[20];
        char state[5];
        int ppid;

        // Inicializa o processo root (init)
        procs[0] = (pproc)malloc(sizeof(proc));
        procs[0]->id = 0;
        procs[0]->pai = 0;
        procs[0]->tabs = -1;
        procs[0]->prox = NULL;
        procs[0]->filho = NULL;
        if (procs[0]->filho == NULL) printf("\n");

        // Abre o diretorio "/proc/"
        if((diretory = opendir( "/proc/" )) != NULL)
        {
                // Entra em cada pasta do diretorio
                diretory_item = readdir( diretory );
                while( diretory_item )
                {
                        // Ignora todas as pastas que não sao de processo
                        if (*(diretory_item->d_name) == '1')
                        {
                                proc = 1;
                        }

                        if (proc) {
                                process[0] = '\0';
                                strcat(process, prefix);
                                strcat(process, diretory_item->d_name);
                                strcat(process, sufix);

                                // Para cada processo le o arquivo "stat" e
                                // armazema os dados do processo na arvore
                                if((fp = fopen(process,"r")) != NULL)
                                {
                                        if(fgets(buffer,MAXBUFF,fp) != NULL)
                                        {
                                                pid = atoi(strtok (buffer," "));
                                                strcpy(name, strtok (NULL," ()"));
                                                strcpy(state, strtok (NULL," ()"));
                                                ppid  = atoi(strtok (NULL," "));

                                                insere (pid, ppid, name, state);
                                        }
                                        if(fp) fclose(fp);
                                }
                        }
                        diretory_item = readdir( diretory );
                }

                // imprime a arvore de processos
                imprime (procs[1]);
        }



        return 0;
}

int main (void)
{
        myps();

        return 0;
}
