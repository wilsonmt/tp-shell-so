#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

/*       Sinais Linux "KILL"

SIGHUP	        1	Hangup
SIGINT	        2	Interrupt from keyboard
SIGKILL	        9	Kill signal
SIGTERM	        15	Termination signal
SIGSTOP	     17,19,23	Stop the process
*/

int sinal (int pid, int sig)
{
        if (sig == 1)
        {
                return kill(pid, SIGHUP);
        }
        else if (sig == 2)
        {
                return kill(pid, SIGINT);
        }
        else if (sig == 9)
        {
                return kill(pid, SIGKILL);
        }
        else if (sig == 15)
        {
                return kill(pid, SIGTERM);
        }
        else if (sig == 17 || sig == 19 || sig == 23)
        {
                return kill(pid, SIGSTOP);
        }
        else return 1;
}

int main()
{
        char sinais [24][10];
        strcpy(sinais[1], "SIGHUP");
        strcpy(sinais[2], "SIGINT");
        strcpy(sinais[9], "SIGKILL");
        strcpy(sinais[15], "SIGTERM");
        strcpy(sinais[17], "SIGSTOP");
        strcpy(sinais[19], "SIGSTOP");
        strcpy(sinais[23], "SIGSTOP");

        int pid, sig, err;

        printf("Sinais aceitos:\n\n \
        SIGHUP          1               Hangup\n \
        SIGINT          2               Interrupt from keyboard\n \
        SIGKILL         9               Kill signal\n \
        SIGTERM         15              Termination signal\n \
        SIGSTOP         17,19,23        Stop the process\n\n" );

        while (1)
        {
                printf("Digite o PID e o sinal(numero) ou 0 0 (zeros) para sair:\n");
                scanf("%d %d", &pid, &sig);
                if (pid == 0) return 0;
                err = sinal(pid, sig);
                if (err == 0) {
                        printf("Sinal %s enviado com sucesso.\n\n", sinais[sig]);
                }
                else if (err == 1) {
                        printf("ERRO: Sinal invalido, tente novamente.\n\n");
                }
                else if (err == -1) {
                        printf("ERRO: PID %d protegido ou inexistente, \ntente novamente.\n\n", pid);
                }
        }
        return 0;
}
